/*
 *                             The MIT License
 *
 * Wavefront Alignments Algorithms
 * Copyright (c) 2017 by Santiago Marco-Sola  <santiagomsola@gmail.com>
 *
 * This file is part of Wavefront Alignments Algorithms.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * PROJECT: Wavefront Alignments Algorithms
 * AUTHOR(S): Santiago Marco-Sola <santiagomsola@gmail.com>
 * DESCRIPTION: Dynamic-programming alignment algorithm for computing
 *   gap-lineal pairwise alignment (Smith-Waterman SW)
 */

#include "sw.h"

/*
 * SW Traceback
 */
void sw_traceback(
    score_matrix_t* const score_matrix,
    const int min_penalty_h,
    const int min_penalty_v,
    lineal_penalties_t* const penalties,
    cigar_t* const cigar) {
  // Parameters
  int** const dp = score_matrix->columns;
  char* const operations = cigar->operations;
  cigar->end_offset = cigar->max_operations;
  int op_sentinel = cigar->end_offset - 1;
  int h = min_penalty_h;
  int v = min_penalty_v;
  //  // Add trailing indels
  //  int h = text_length;
  //  int v = pattern_length;
  //  while (h > min_penalty_h) {operations[op_sentinel--] = 'I'; --h;}
  //  while (v > min_penalty_v) {operations[op_sentinel--] = 'D'; --v;}
  // Compute traceback
  while (h>0 && v>0) {
    // Check score
    if (dp[h][v] == 0) break;
    // Backtrace step
    if (dp[h][v] == dp[h][v-1]+penalties->deletion) {
      operations[op_sentinel--] = 'D';
      --v;
    } else if (dp[h][v] == dp[h-1][v]+penalties->insertion) {
      operations[op_sentinel--] = 'I';
      --h;
    } else {
      operations[op_sentinel--] =
          (dp[h][v] == dp[h-1][v-1]+penalties->match) ? 'M' : 'X';
      --h;
      --v;
    }
  }
  //  // Add leading indels
  //  while (h > 0) {operations[op_sentinel--] = 'I'; --h;}
  //  while (v > 0) {operations[op_sentinel--] = 'D'; --v;}
  cigar->begin_offset = op_sentinel + 1;
}
/*
 * SW computation using raw dynamic-programming matrix
 */
int sw_compute(
    score_matrix_t* const score_matrix,
    lineal_penalties_t* const penalties,
    const char* const pattern,
    const int pattern_length,
    const char* const text,
    const int text_length,
    cigar_t* const cigar) {
  // Parameters
  int** dp = score_matrix->columns;
  int h, v;
  // Initialize (ends-free)
  dp[0][0] = 0;
  for (v=1;v<=pattern_length;++v) dp[0][v] = 0;
  for (h=1;h<=text_length;++h) dp[h][0] = 0;
  // Compute DP
  int min_penalty = 0, min_penalty_h = 0, min_penalty_v = 0;
  for (h=1;h<=text_length;++h) {
    for (v=1;v<=pattern_length;++v) {
      const char pattern_char = pattern[v-1];
      const char text_char = text[h-1];
      const int misms_penalty = penalties->mismatch[(int)pattern_char][(int)text_char];
      int min = dp[h-1][v-1] + ((pattern_char==text_char) ? penalties->match : misms_penalty); // Misms
      min = MIN(min,dp[h-1][v]+penalties->insertion); // Ins
      min = MIN(min,dp[h][v-1]+penalties->deletion); // Del
      min = MIN(min,0);
      dp[h][v] = min;
      if (min < min_penalty) {
        min_penalty = min;
        min_penalty_h = h;
        min_penalty_v = v;
      }
    }
  }
  // Compute traceback
  sw_traceback(score_matrix,min_penalty_h,min_penalty_v,penalties,cigar);
  // Return score
  return -min_penalty;
}
/*
 * SW computation using raw dynamic-programming matrix (banded)
 */
int sw_compute_banded(
    score_matrix_t* const score_matrix,
    lineal_penalties_t* const penalties,
    const char* const pattern,
    const int pattern_length,
    const char* const text,
    const int text_length,
    const int bandwidth,
    cigar_t* const cigar) {
  // Parameters
  const int k_end = ABS(text_length-pattern_length)+1;
  const int effective_bandwidth = MAX(k_end,bandwidth);
  int** dp = score_matrix->columns;
  int h, v;
  // Initialize (ends-free)
  dp[0][0] = 0;
  for (v=1;v<=effective_bandwidth;++v) dp[0][v] = 0;
  // Compute DP
  int min_penalty = 0, min_penalty_h = 0, min_penalty_v = 0;
  for (h=1;h<=text_length;++h) {
    // Compute lo limit
    const bool lo_band = (h <= effective_bandwidth);
    const int lo = (lo_band) ? 1 : h - effective_bandwidth;
    dp[h][lo-1] = (lo_band) ? 0 : INT16_MAX;
    // Compute hi limit
    const int hi = MIN(pattern_length,effective_bandwidth+h-1);
    if (h > 1) dp[h-1][hi] = INT16_MAX;
    // Compute column
    for (v=lo;v<=hi;++v) {
      const char pattern_char = pattern[v-1];
      const char text_char = text[h-1];
      const int misms_penalty = penalties->mismatch[(int)pattern_char][(int)text_char];
      int min = dp[h-1][v-1] + ((pattern_char==text_char) ? penalties->match : misms_penalty); // Misms
      min = MIN(min,dp[h-1][v]+penalties->insertion); // Ins
      min = MIN(min,dp[h][v-1]+penalties->deletion); // Del
      min = MIN(min,0);
      dp[h][v] = min;
      if (min < min_penalty) {
        min_penalty = min;
        min_penalty_h = h;
        min_penalty_v = v;
      }
    }
  }
  // Compute traceback
  sw_traceback(score_matrix,min_penalty_h,min_penalty_v,penalties,cigar);
  // Return score
  return -min_penalty;
}

