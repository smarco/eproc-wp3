/*
 *                             The MIT License
 *
 * Genomic Tools & Algorithms
 * Copyright (c) 2017 by Santiago Marco-Sola  <santiagomsola@gmail.com>
 *
 * This file is part of Genomic Tools & Algorithms.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * PROJECT: Genomic Tools & Algorithms
 * AUTHOR(S): Santiago Marco-Sola <santiagomsola@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

/*
 * Config
 */
#define REPS 100000000

/*
 * Utils
 */
#define MIN(a,b) (((a)<=(b))?(a):(b))
#define MAX(a,b) (((a)>=(b))?(a):(b))
#define ABS(a) (((a)>=0)?(a):-(a))

#define DIV_FLOOR(NUMERATOR,DENOMINATOR)  ((NUMERATOR)/(DENOMINATOR))
#define DIV_CEIL(NUMERATOR,DENOMINATOR)   (((NUMERATOR)+((DENOMINATOR)-1))/(DENOMINATOR))

/*
 * Input Sequence
 */
uint64_t packed_seq = 0xEA550000EA550000ul;

/*
 * Block Pack (Instruction)
 */
void bcnt(
    uint64_t* const rd,
    const uint64_t rs1,
    const uint64_t rs2) {
  // Init rd
  uint16_t* const rd16 = (uint16_t*)rd;
  rd16[0] = 0;
  rd16[1] = 0;
  rd16[2] = 0;
  rd16[3] = 0;
  // Compute offset
  int offset = rs2 & 31;
  // Count up to offset (exclusive)
  int i;
  for (i=0;i<offset;++i) {
    const int idx = (rs1 >> (2*i)) & 3;
    rd16[idx]++;
  }
}

/*
 * Generic Menu
 */
int main(int argc,char* argv[]) {
  // Benchmark
  uint16_t counts[4];
  int rep, j;
  for (rep=0;rep<REPS;++rep) {
    // Count bases
    bcnt((uint64_t*)counts,packed_seq,1040);
  }

  fprintf(stderr,">>> COUNTS\n");
  fprintf(stderr," A = %d\n",counts[0]);
  fprintf(stderr," C = %d\n",counts[1]);
  fprintf(stderr," G = %d\n",counts[3]);
  fprintf(stderr," T = %d\n",counts[2]);

  // Exit
  return 0;
}



