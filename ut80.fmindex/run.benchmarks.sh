#!/bin/bash

# Need five arguments : ref_file query_set batch_size minSeedLen n_threads
./fmi data/references/yeast/yeast data/queries/yeast/input.small.fastq 512 19 1 > data/queries/yeast/input.small.output
./fmi data/references/yeast/yeast data/queries/yeast/input.large.fastq 512 19 1 > data/queries/yeast/input.large.output

./fmi data/references/hs.chr1/chr1.fasta data/queries/hs.chr1/input.small.fastq 512 19 1 > data/queries/hs.chr1/input.small.out
./fmi data/references/hs.chr1/chr1.fasta data/queries/hs.chr1/input.large.fastq 512 19 1 > data/queries/hs.chr1/input.large.out
