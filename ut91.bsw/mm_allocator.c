/*
 *                             The MIT License
 *
 * Wavefront Alignments Algorithms
 * Copyright (c) 2017 by Santiago Marco-Sola  <santiagomsola@gmail.com>
 *
 * This file is part of Wavefront Alignments Algorithms.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * PROJECT: Wavefront Alignments Algorithms
 * AUTHOR(S): Santiago Marco-Sola <santiagomsola@gmail.com>
 * DESCRIPTION: Simple managed-memory allocator that reduces the overhead
 *   of using malloc/calloc/free functions by allocating slabs of memory
 *   and dispatching memory segments in order.
 */

#include "mm_allocator.h"

/*
 * Constants
 */
#define MM_ALLOCATOR_SEGMENT_INITIAL_REQUESTS   10000
#define MM_ALLOCATOR_INITIAL_SEGMENTS              10
#define MM_ALLOCATOR_INITIAL_MALLOC_REQUESTS       10
#define MM_ALLOCATOR_INITIAL_STATES                10

/*
 * Allocator Segments Freed Cond
 */
#define MM_ALLOCATOR_FREED_FLAG                 0x80000000ul
#define MM_ALLOCATOR_REQUEST_IS_FREE(request)  ((request)->size & MM_ALLOCATOR_FREED_FLAG)
#define MM_ALLOCATOR_REQUEST_SET_FREE(request) ((request)->size |= MM_ALLOCATOR_FREED_FLAG)
#define MM_ALLOCATOR_REQUEST_SIZE(request)     ((request)->size & ~(MM_ALLOCATOR_FREED_FLAG))

/*
 * Reference (Header of every memory allocated)
 */
typedef uint32_t mm_allocator_reference_t;

/*
 * Setup
 */
void mm_allocator_init(
    mm_allocator_t* const mm_allocator,
    void* const segment_mem,
    const uint64_t segment_size) {
  // Allocate handler
  mm_allocator->request_ticker = 0;
  // Segments
  mm_allocator->segment_size = segment_size;
  mm_allocator->segment_mem = segment_mem;
  mm_allocator->segment_used = 0;
  // Requests
  mm_allocator->requests_used = 0;
}
void mm_allocator_clear(
    mm_allocator_t* const mm_allocator) {
  mm_allocator->segment_used = 0;
  mm_allocator->requests_used = 0;
}
/*
 * Allocate
 */
void* mm_allocator_allocate(
    mm_allocator_t* const mm_allocator,
    const uint64_t num_bytes,
    const bool zero_mem,
    const uint64_t align_bytes
#ifdef MM_ALLOCATOR_LOG
    ,const char* func_name,
    uint64_t line_no
#endif
    ) {
  // Zero check
  if (num_bytes == 0) {
    printf("MMAllocator error. Zero bytes requested\n");
    exit(1);
  }
  // Add payload and check occupancy
  const uint64_t num_bytes_allocated = num_bytes + sizeof(mm_allocator_reference_t) + align_bytes;
  if (mm_allocator->segment_used + num_bytes_allocated > mm_allocator->segment_size) {
    printf("MMAllocator error. Not enough memory\n");
    exit(1);
  }
  if (mm_allocator->requests_used >= MM_ALLOCATOR_MAX_REQUESTS) {
    printf("MMAllocator error. Not enough memory for requests\n");
    exit(1);
  }
  // Allocate memory
  void* const memory_base = mm_allocator->segment_mem + mm_allocator->segment_used;
  if (zero_mem) memset(memory_base,0,num_bytes_allocated); // Set zero
  // Compute aligned memory
  void* memory_aligned = memory_base + sizeof(mm_allocator_reference_t) + align_bytes;
  if (align_bytes > 0) {
    memory_aligned = memory_aligned - ((uintptr_t)memory_aligned % align_bytes);
  }
  // Set mm_reference
  mm_allocator_reference_t* const mm_reference = memory_aligned - sizeof(mm_allocator_reference_t);
  *mm_reference = mm_allocator->requests_used;
  // Add request
  mm_allocator_request_t* const request = mm_allocator->requests + mm_allocator->requests_used;
  request->offset = mm_allocator->segment_used;
  request->size = num_bytes_allocated;
#ifdef MM_ALLOCATOR_LOG
  request->timestamp = (mm_allocator->request_ticker)++;
  request->func_name = (char*)func_name;
  request->line_no = line_no;
#endif
  // Update request and occupation
  ++(mm_allocator->requests_used);
  mm_allocator->segment_used += num_bytes_allocated;
  // Return memory
  return memory_aligned;
}
/*
 * Allocator Free
 */
void mm_allocator_free(
    mm_allocator_t* const mm_allocator,
    void* const memory) {
  // Get reference and request
  void* const effective_memory = memory - sizeof(mm_allocator_reference_t);
  mm_allocator_reference_t* const mm_reference = effective_memory;
  mm_allocator_request_t* const request = mm_allocator->requests + *mm_reference;
  // Check double-free
  if (MM_ALLOCATOR_REQUEST_IS_FREE(request)) {
    printf("MMAllocator error: double free\n");
    exit(1);
  }
  // Free request
  MM_ALLOCATOR_REQUEST_SET_FREE(request);
  // Free contiguous request(s) at the end of the segment
  uint64_t num_requests = mm_allocator->requests_used;
  if (*mm_reference == num_requests-1) { // Is the last request?
    --num_requests;
    mm_allocator_request_t* request = mm_allocator->requests + (num_requests-1);
    while (num_requests>0 && MM_ALLOCATOR_REQUEST_IS_FREE(request)) {
      --num_requests; // Free request
      --request;
    }
    // Update segment used
    if (num_requests > 0) {
      mm_allocator->segment_used = request->offset + request->size;
      mm_allocator->requests_used = num_requests;
    } else {
      // All freed
      mm_allocator_clear(mm_allocator);
    }
  }
}
/*
 * Utils
 */
void mm_allocator_get_occupation(
    mm_allocator_t* const mm_allocator,
    uint64_t* const bytes_used_allocator,
    uint64_t* const bytes_free_available,
    uint64_t* const bytes_free_fragmented) {
  // Init
  *bytes_used_allocator = 0;
  *bytes_free_available = 0;
  *bytes_free_fragmented = 0;
  // Check allocator memory
  const uint64_t num_requests = mm_allocator->requests_used;
  int64_t request_idx;
  bool free_memory = true;
  for (request_idx=num_requests-1;request_idx>=0;--request_idx) {
    mm_allocator_request_t* const request = mm_allocator->requests + request_idx;
    const uint64_t size = MM_ALLOCATOR_REQUEST_SIZE(request);
    if (MM_ALLOCATOR_REQUEST_IS_FREE(request)) {
      if (free_memory) {
        *bytes_free_available += size;
      } else {
        *bytes_free_fragmented += size;
      }
    } else {
      free_memory = false;
      *bytes_used_allocator += size;
    }
  }
  // Account for free space at the end of the segment
  if (num_requests > 0) {
    mm_allocator_request_t* const request = mm_allocator->requests + (num_requests-1);
    *bytes_free_available += mm_allocator->segment_size - (request->offset+request->size);
  } else {
    *bytes_free_available = mm_allocator->segment_size;
  }
}
/*
 * Display
 */
void mm_allocator_print_allocator_request(
    FILE* const stream,
    mm_allocator_request_t* const request,
    const uint64_t segment_idx,
    const uint64_t request_idx) {
  printf("    [#%03" PRIu64 "/%05" PRIu64 "\t%s\t@%08u\t(%" PRIu64 " Bytes)"
#ifdef MM_ALLOCATOR_LOG
          "\t%s:%" PRIu64 "\t{ts=%" PRIu64 "}"
#endif
          "\n",
          segment_idx,
          request_idx,
          MM_ALLOCATOR_REQUEST_IS_FREE(request) ? "Free]     " : "Allocated]",
          request->offset,
          (uint64_t)MM_ALLOCATOR_REQUEST_SIZE(request)
#ifdef MM_ALLOCATOR_LOG
          ,request->func_name,
          request->line_no,
          request->timestamp
#endif
      );
}
void mm_allocator_print_allocator_requests(
    FILE* const stream,
    mm_allocator_t* const mm_allocator,
    const bool compact_free) {
  // Print allocator memory
  uint64_t segment_idx = 0, request_idx = 0;
  uint64_t free_block = 0;
  bool has_requests = false;
  printf("  => MMAllocator.requests\n");
  const uint64_t requests_used = mm_allocator->requests_used;
  for (request_idx=0;request_idx<requests_used;++request_idx) {
    mm_allocator_request_t* const request = mm_allocator->requests + request_idx;
    if (compact_free) {
      if (MM_ALLOCATOR_REQUEST_IS_FREE(request)) {
        free_block += MM_ALLOCATOR_REQUEST_SIZE(request);
      } else {
        if (free_block > 0) {
          printf("    [n/a\tFree]      \t(%" PRIu64 " Bytes)\n",free_block);
          free_block = 0;
        }
        mm_allocator_print_allocator_request(stream,request,segment_idx,request_idx);
        has_requests = true;
      }
    } else {
      mm_allocator_print_allocator_request(stream,request,segment_idx,request_idx);
      has_requests = true;
    }
  }
  if (!has_requests) {
    printf("    -- No requests --\n");
  }
}
void mm_allocator_print(
    FILE* const stream,
    mm_allocator_t* const mm_allocator,
    const bool display_requests) {
  // Print header
  printf("MMAllocator.report\n");
  // Print segment information
  const uint64_t segment_size = mm_allocator->segment_size;
  printf("  => Segments.allocated %" PRIu64 "\n",1ul);
  printf("  => Segments.size      %" PRIu64 " MB\n",segment_size/(1024*1024));
  printf("  => Memory.available   %" PRIu64 " MB\n",1*(segment_size/(1024*1024)));
  // Print memory information
  uint64_t bytes_used_allocator, bytes_free_available, bytes_free_fragmented;
  mm_allocator_get_occupation(mm_allocator,&bytes_used_allocator,&bytes_free_available,&bytes_free_fragmented);
  printf("    => Memory.used   %" PRIu64 "\n",bytes_used_allocator);
  printf("    => Memory.free   %" PRIu64 "\n",bytes_free_available+bytes_free_fragmented);
  printf("      => Memory.free.available  %" PRIu64 "\n",bytes_free_available);
  printf("      => Memory.free.fragmented %" PRIu64 "\n",bytes_free_fragmented);
  // Print memory requests
  if (display_requests) {
    mm_allocator_print_allocator_requests(stream,mm_allocator,false);
  }
}



