/*
 *                             The MIT License
 *
 * Wavefront Alignments Algorithms
 * Copyright (c) 2017 by Santiago Marco-Sola  <santiagomsola@gmail.com>
 *
 * This file is part of Wavefront Alignments Algorithms.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * PROJECT: Wavefront Alignments Algorithms
 * AUTHOR(S): Santiago Marco-Sola <santiagomsola@gmail.com>
 * DESCRIPTION: Wavefront Alignment benchmarking tool
 */

// DEBUG
#ifdef DISABLE_PRINT
  #define printf(fmt, ...)
  #define fprintf(fmt, ...)
#endif
// PMU
#ifdef PMU
  #include "../common/pmu.h"
  #define PROFILE_RESET() reset_pmu()
  #define PROFILE_START() enable_PMU_32b()
  #define PROFILE_STOP()  disable_PMU_32b()
  #define PROFILE_PRINT() print_PMU_events()
#else
  #define PROFILE_RESET()
  #define PROFILE_START()
  #define PROFILE_STOP()
  #define PROFILE_PRINT()
#endif

// Includes
#include "commons.h"
#include "cigar.h"
#include "sw.h"

// Input
typedef struct {
  char *pattern;
  char *text;
  int64_t text_length;
  int64_t pattern_length;
  int64_t score;
} input_sequence_t;
#define MAX_SEQUENCE_LENGTH 1100

// SELECT INPUT, REPETITIONS, AND BAND-WIDTH HERE
#define REPS 1
#define SW_BANDWIDTH 10
//#define SW_BANDWIDTH 20
//#define SW_BANDWIDTH 30
#include "data/sequences.n10K.l100.h"
//#define SW_BANDWIDTH 100
//#define SW_BANDWIDTH 200
//#define SW_BANDWIDTH 300
//#include "data/sequences.n1K.l1K.h"

// Memory
#define MM_ALLOCATOR_MEM_SIZE BUFFER_SIZE_64M
uint8_t mm_allocator_mem[MM_ALLOCATOR_MEM_SIZE];

/*
 * Generic parameters
 */
typedef struct {
  // Misc
  int progress;
  bool verbose;
} benchmark_args;
benchmark_args parameters = {
  // Misc
  .progress = 10000,
  .verbose = false
};

/*
 * Generic Menu
 */
int main(int argc,char* argv[]) {
  // Initialize
  mm_allocator_t mm_allocator;
  mm_allocator_init(&mm_allocator,mm_allocator_mem,MM_ALLOCATOR_MEM_SIZE);
  score_matrix_t score_matrix;
  score_matrix_allocate(&score_matrix,MAX_SEQUENCE_LENGTH,MAX_SEQUENCE_LENGTH,&mm_allocator);
  lineal_penalties_t penalties = {
      .match = -1,
      .insertion = 2,
      .deletion = 2,
      .mismatch['A']['A'] =-1, .mismatch['A']['C'] = 3, .mismatch['A']['G'] = 3, .mismatch['A']['T'] = 3,
      .mismatch['C']['A'] = 3, .mismatch['C']['C'] =-1, .mismatch['C']['G'] = 3, .mismatch['C']['T'] = 3,
      .mismatch['G']['A'] = 3, .mismatch['G']['C'] = 3, .mismatch['G']['G'] =-1, .mismatch['G']['T'] = 3,
      .mismatch['T']['A'] = 3, .mismatch['T']['C'] = 3, .mismatch['T']['G'] = 3, .mismatch['T']['T'] =-1,
  };
  cigar_t cigar;
  cigar_allocate(&cigar,2*MAX_SEQUENCE_LENGTH+1,&mm_allocator);
  // PMU
  PROFILE_RESET();
  // Read-align loop
  int rep, correct = 1;
  for (rep=0;rep<REPS;++rep) {
    int i, progress = 0;
    printf("Doing repetition %d \n",rep);
    for (i=0;i<num_input_sequences;++i) {
      // Align
      const int64_t score = sw_compute_banded(
          &score_matrix,&penalties,
          input_sequences[i].pattern,
          input_sequences[i].pattern_length,
          input_sequences[i].text,
          input_sequences[i].text_length,SW_BANDWIDTH,&cigar);
      // Check
      if (score != input_sequences[i].score) {
        printf("[ERROR] Incorrect alignment of sequence %d"
               " (score = %ld, correct=%ld)\n",i,score,input_sequences[i].score);
        printf(">%s\n",input_sequences[i].pattern);
        printf("<%s\n",input_sequences[i].text);
        correct = 0;
      }
      // Update progress
      if (++progress == parameters.progress) {
        progress = 0;
        printf("...processed %d reads \n",i+1);
      }
    }
  }
  // PMU
  PROFILE_PRINT();
  // DEBUG
  if (correct) {
    printf("All sequences aligned correctly! (%d sequences)\n",num_input_sequences);
  } else {
    printf("Errors aligning sequences\n");
  }
  // Free
  score_matrix_free(&score_matrix);
  // Exit
  return 0;
}
